from django.db import models


class Employee(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)


class Shift(models.Model):
    start = models.DateTimeField('start datetime')
    end = models.DateTimeField('end datetime')
    break_mins = models.IntegerField()
    employee = models.ForeignKey(Employee, null=True, default=None, on_delete=models.SET_DEFAULT)
