from rest_framework import viewsets
from rest_framework.response import Response

from be_app.models import Employee, Shift
from be_app.serializers import EmployeeSerializer, ShiftSerializer


class EmployeeViewSet(viewsets.ModelViewSet):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer


class ShiftViewSet(viewsets.ModelViewSet):
    queryset = Shift.objects.all()
    serializer_class = ShiftSerializer


# This API allocates shifts to employees and returns the result.
# It does NOT currently save it in the database, it is for review.
# This could easily be implemented (ie shift.save()) with a commit flag in the request, or maybe using a POST.

class AllocateViewSet(viewsets.ViewSet):
    def list(self, request):
        shifts = Shift.objects.all()
        employees = Employee.objects.all()

        allocated = allocate_shifts(employees, shifts)

        serializer = ShiftSerializer(allocated, many=True, context={'request': request})
        return Response(serializer.data)


# Allocates shifts round-robin which avoids working back-to-back, but otherwise is not ideal for humans.

def allocate_shifts(employees, shifts):
    index = 0
    for s in shifts:
        s.employee = employees[index]
        # TODO logging
        print("shift alloc to {}".format(employees[index].first_name))
        index += 1
        if index >= len(employees):
            index = 0

    return shifts
