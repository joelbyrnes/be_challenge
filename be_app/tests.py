from django.test import TestCase
from django.utils import dateparse
from rest_framework import status
from rest_framework.test import APITestCase

from be_app.models import Shift, Employee
from be_app.views import allocate_shifts


class ShiftModelTests(TestCase):
    def test_create_shift(self):
        count = Shift.objects.count()
        Shift.objects.create(start=dateparse.parse_datetime('2018-06-18T05:00:00.000Z'),
                             end=dateparse.parse_datetime('2018-06-18T13:30:00.000Z'),
                             break_mins=60)
        self.assertEqual(Shift.objects.count(), count+1)
        self.assertEqual(Shift.objects.all()[0].employee, None)


class ShiftAPITests(APITestCase):
    def test_get_all(self):
        response = self.client.get('/shifts/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_create_shift(self):
        data = dict(start=dateparse.parse_datetime('2018-06-18T05:00:00.000Z'),
                    end=dateparse.parse_datetime('2018-06-18T13:30:00.000Z'),
                    break_mins=60)

        count = Shift.objects.count()
        response = self.client.post('/shifts/', data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Shift.objects.count(), count+1)


class AllocateAPITests(APITestCase):
    def test_allocate_get(self):
        Shift.objects.create(start=dateparse.parse_datetime('2018-06-18T05:00:00.000Z'),
                             end=dateparse.parse_datetime('2018-06-18T13:30:00.000Z'),
                             break_mins=60)
        Shift.objects.create(start=dateparse.parse_datetime('2018-06-18T05:00:00.000Z'),
                             end=dateparse.parse_datetime('2018-06-18T13:30:00.000Z'),
                             break_mins=60)

        Employee.objects.create(first_name="Alice", last_name="Smith")
        Employee.objects.create(first_name="Bob", last_name="Smith")
        Employee.objects.create(first_name="Carol", last_name="Smith")

        response = self.client.get('/allocate/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = response.render().json()
        self.assertEqual(data[0]['employee'], 'http://testserver/employees/1/')
        self.assertEqual(data[1]['employee'], 'http://testserver/employees/2/')


class AllocationTests(TestCase):
    def test_allocate_basic(self):
        shifts = [Shift.objects.create(start=dateparse.parse_datetime('2018-06-18T05:00:00.000Z'),
                                       end=dateparse.parse_datetime('2018-06-18T13:30:00.000Z'),
                                       break_mins=60),
                  Shift.objects.create(start=dateparse.parse_datetime('2018-06-18T05:00:00.000Z'),
                                       end=dateparse.parse_datetime('2018-06-18T13:30:00.000Z'),
                                       break_mins=60)]

        employees = [Employee.objects.create(first_name="Alice", last_name="Smith"),
                     Employee.objects.create(first_name="Bob", last_name="Smith"),
                     Employee.objects.create(first_name="Carol", last_name="Smith")]

        allocated = allocate_shifts(employees, shifts)
        self.assertEqual(allocated[0].employee, employees[0])
        self.assertEqual(allocated[1].employee, employees[1])
