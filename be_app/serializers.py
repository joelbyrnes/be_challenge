from rest_framework import serializers

from be_app.models import Employee, Shift


class EmployeeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Employee
        fields = ('first_name', 'last_name')


class ShiftSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Shift
        fields = ('start', 'end', 'break_mins', 'employee')
