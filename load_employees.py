import csv
import requests
import pprint

# This script exists to load employees from CSV into the database via API, to keep the web server simple and decoupled.

# Caveats:
# * There is currently no duplicate checking so it should only be run once.


def employees():
    r = requests.get("http://localhost:8000/employees/")
    r.raise_for_status()
    return r.json()


def add_employee(data):
    r = requests.post("http://localhost:8000/employees/", data=data)
    r.raise_for_status()
    return r.json()


input = []
with open("files/employees.csv") as f:
    reader = csv.DictReader(f, ['first_name', 'last_name'])
    next(reader.reader)   # skip header row 
    for row in reader:
        input.append(row)

for u in input:
    add_employee(u)

pprint.pprint(employees())
