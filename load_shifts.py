import csv
from datetime import datetime, timedelta

import requests
import pprint

from django.utils import dateparse

# This script exists to load shifts from CSV into the database via API, to keep the web server simple and decoupled.
# If appropriate the logic could be moved into the web server side.

# Caveats:
# * There is currently no duplicate checking so it should only be run once.
# * There is no validation of the file/data other than meeting CSV format.
# * No validation of dates, such as being sequential, checking for large overlaps, etc.
# * It converts the CSV date and start/end to two datetime fields to make calculations easier later.
# * No timezone is listed so assuming local time of the server.


def shifts():
    r = requests.get("http://localhost:8000/shifts/")
    r.raise_for_status()
    return r.json()


def add_shift(data):
    r = requests.post("http://localhost:8000/shifts/", data=data)
    r.raise_for_status()
    return r.json()


input = []
with open("files/shifts.csv") as f:
    reader = csv.DictReader(f, ['date', 'start', 'end', 'break_mins'])
    next(reader.reader)   # skip header row
    for row in reader:
        input.append(row)

for s in input:
    print(s['date'], s['start'], s['end'], s['break_mins'])
    date=datetime.strptime(s['date'], "%d/%m/%Y")
    start=datetime.strptime(s['start'], "%I:%M:%S %p").time()
    end=datetime.strptime(s['end'], "%I:%M:%S %p").time()

    start_dt = datetime.combine(date, start)

    if end > start:
        end_dt = datetime.combine(date, end)
    else:
        end_dt = datetime.combine(date + timedelta(days=1), end)

    shift = dict(start=start_dt, end=end_dt, break_mins=s['break_mins'])

    add_shift(shift)


pprint.pprint(shifts())
